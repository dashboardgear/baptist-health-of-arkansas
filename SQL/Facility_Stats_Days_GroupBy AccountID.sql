/*	Facility_Stats_Days_GroupBy AccountID.sql
	6/19/14
*/

DECLARE @begin date
DECLARE @end date
SET @begin='2014-02-01'
SET @end= '2014-02-28';

SELECT --top 100 *
	CASE
		WHEN LOCATION='10205' THEN 'LTAC'
		WHEN LOCATION='10201' THEN 'Rehab'
		WHEN COST_CENTER_CODE='3007133' THEN 'GeriPsych'
		WHEN REV_CODE=174 THEN 'NICU'
		WHEN REV_CODE IN (128,118) THEN 'REHAB'
		WHEN REV_CODE IN (170, 171, 172, 173, 179) THEN 'NEWBORN'
		WHEN REV_CODE BETWEEN 191 AND 194 THEN 'SNF/SWINGBED'
		ELSE 'Adults&Peds'
		--ELSE 'Unassigned'
	END AS SERVICE_LINE,
	ACCOUNT_ID,
	SUM(QUANTITY) AS TOT_QTY,
	@begin AS BEGIN_DATE,
	@end AS END_DATE
FROM
	BH_EPIC_CHARGES
WHERE
	REV_CODE BETWEEN 100 AND 249
	AND SERVICE_DATE BETWEEN @begin AND @end
GROUP BY 	
	CASE
		WHEN LOCATION='10205' THEN 'LTAC'
		WHEN LOCATION='10201' THEN 'Rehab'
		WHEN COST_CENTER_CODE='3007133' THEN 'GeriPsych'
		WHEN REV_CODE=174 THEN 'NICU'
		WHEN REV_CODE IN (128,118) THEN 'REHAB'
		WHEN REV_CODE IN (170, 171, 172, 173, 179) THEN 'NEWBORN'
		WHEN REV_CODE BETWEEN 191 AND 194 THEN 'SNF/SWINGBED'
		ELSE 'Adults&Peds'
		--ELSE 'Unassigned'
	END,
	ACCOUNT_ID
ORDER BY
	CASE
		WHEN LOCATION='10205' THEN 'LTAC'
		WHEN LOCATION='10201' THEN 'Rehab'
		WHEN COST_CENTER_CODE='3007133' THEN 'GeriPsych'
		WHEN REV_CODE=174 THEN 'NICU'
		WHEN REV_CODE IN (128,118) THEN 'REHAB'
		WHEN REV_CODE IN (170, 171, 172, 173, 179) THEN 'NEWBORN'
		WHEN REV_CODE BETWEEN 191 AND 194 THEN 'SNF/SWINGBED'
		ELSE 'Adults&Peds'
		--ELSE 'Unassigned'
	END,
	ACCOUNT_ID
