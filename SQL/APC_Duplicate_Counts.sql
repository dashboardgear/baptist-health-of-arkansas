/*	APC_Duplicate_Counts.sql
*/


WITH COUNT AS (
SELECT
	ACCOUNT_ID,
	ROW_NUMBER() OVER (PARTITION BY 
		ACCOUNT_ID
      ,APC_CODE
      ,APC_STATUS
      ,HCPCS_CODE
      ,APC_MODIFIERS
      ,APC_CLM_CHG_AMT
      ,APC_CLM_QTY
      ,APC_CLM_LN_NBR
      ,APC_REIMBURSEMENT
      ,APC_SERVICE_DATE
      ,APC_REV_CODE
      ,ACCT_BASE_CLASS 
		ORDER BY 
			ACCOUNT_ID
      ,APC_CODE
      ,APC_STATUS
      ,HCPCS_CODE
      ,APC_MODIFIERS
      ,APC_CLM_CHG_AMT
      ,APC_CLM_QTY
      ,APC_CLM_LN_NBR
      ,APC_REIMBURSEMENT
      ,APC_SERVICE_DATE
      ,APC_REV_CODE
      ,ACCT_BASE_CLASS
) AS RowNum,
	HCPCS_CODE,
	APC_SERVICE_DATE,
	SOURCE_DATE
FROM BH_EPIC_APC
)
--SELECT DISTINCT SOURCE_DATE, COUNT(SOURCE_DATE) AS SOURCE_COUNT
--FROM COUNT
--WHERE RowNum>1
--GROUP BY SOURCE_DATE
	
SELECT
	ACCOUNT_ID,
	RowNum,
	HCPCS_CODE,
	SOURCE_DATE
FROM COUNT
WHERE
	RowNum>1
	AND SOURCE_DATE='2014-04-01'

