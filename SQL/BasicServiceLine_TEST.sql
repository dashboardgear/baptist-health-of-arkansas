/*	BasicServiceLines.sql
	Build temp table with Account_ID and SERVICE_LINE_BASIC for
		use in BuildPatientTable procedure
*/

SELECT --TOP 100
	ACCOUNT_ID,
	DEFAULT_REV_CODE,
	SUM(AMOUNT) AS TOT_AMT
INTO #REV_TEMP
FROM BH_EPIC_CHARGES
WHERE
	DEFAULT_REV_CODE ='762'
GROUP BY ACCOUNT_ID, DEFAULT_REV_CODE
HAVING SUM(AMOUNT)>0;
----------------------------------
SELECT
	ACCOUNT_ID,
	COST_CENTER_CODE
INTO #CC_TEMP
FROM BH_EPIC_CHARGES 
GROUP BY ACCOUNT_ID, COST_CENTER_CODE
HAVING SUM(AMOUNT)>0;

----------------- SERVICE_LINE_BASIC ----------------
SELECT
ACCOUNT_ID,
ACCT_BASE_CLASS,
CASE ACCT_BASE_CLASS
WHEN 'IP' THEN 
	CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN 'LTACH'
		WHEN PAT.LOCATION = '10201' THEN 'Rehab'
		WHEN PAT.DISCH_DEPT_CODE = '10102128' THEN 'Rehab' --'NR 1R REHAB'
		WHEN PAT.DISCH_DEPT_CODE IN ('10003110', '10004145','10101253','10005147') THEN 'SNF/Swingbed'
		WHEN PAT.DISCH_DEPT_CODE = '10101241' THEN 'Geri-Psych'
		--WHEN Charges in cost center 3006123 THEN 'NICU' -- Replace next line with this one if possible
		WHEN PAT.DISCH_DEPT_CODE = '10101240' THEN 'NICU'
		ELSE --ISNULL(DRG.BASIC_SERVICE_LINE,'ERROR')
			DRG.BASIC_SERVICE_LINE
	END 
WHEN 'OP' THEN
	CASE --First match based on hierarchy
		WHEN (NOT EXISTS (SELECT ACCOUNT_ID FROM #CC_TEMP)) AND (CHG_O.DEFAULT_REV_CODE!='762') THEN NULL
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Cardiac Cath/EP')))
			THEN 'Cardiac Cath/EP'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Operating Room')))
			THEN 'Operating Dept'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='GI Lab')))
			THEN 'GI Lab'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Interventional Radiology')))
			THEN 'Interventional Radiology'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Vein Center')))
			THEN 'Vein Center'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Sleep Lab')))
			THEN 'Sleep Lab'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Wound Center')))
			THEN 'Wound Center'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='PT,OT,ST')))
			THEN 'PT,OT,ST'
		WHEN CHG_O.DEFAULT_REV_CODE = '762' THEN 'Observation'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Radiology')))
			THEN 'Radiology'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Other')))
			THEN 'Other'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Laboratory')))
			THEN 'Laboratory'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='N/A')))
			THEN 'N/A'		
		ELSE 'Error'
		END
WHEN 'ED' THEN 'ED'
END AS SERVICE_LINE_BASIC

INTO #BASIC_SERVICE_LINE
FROM BH_EPIC_PATIENTS  PAT --#CPT_TEMP CPT
	OUTER APPLY (SELECT TOP 1 DEFAULT_REV_CODE FROM #REV_TEMP  --BH_EPIC_CHARGES 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
			DEFAULT_REV_CODE = '762' -- Observation
		) AS CHG_O
	OUTER APPLY (SELECT DRG, DRG_DESCRIPTION, DRG_WEIGHT, DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_DRG_REFERENCE WHERE 
		DRG=PAT.DRG AND PAT.LOCATION NOT IN ('10205','20102')
		AND ISNULL(PAT.ADMIT_DATE,PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) DRG
	OUTER APPLY (SELECT LTC_DRG, LTC_DRG_DESCRIPTION, LTC_DRG_WEIGHT, LTC_DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_LTACH_REFERENCE WHERE 
		LTC_DRG=PAT.DRG AND PAT.LOCATION IN ('10205','20102')
		AND ISNULL(PAT.ADMIT_DATE,PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) LTC
WHERE 
	PAT.INCLUDE_FLAG='Y'
	AND (DISCH_DEPT_CODE IS NULL OR 
		DISCH_DEPT_CODE NOT IN ('10101201', '10004102', '10003163', '10005160', '10005161', '10005162', '10005163', '10005157', '10005129', '10003138', '10101466', '10002212','10005165'))

	
DROP TABLE #REV_TEMP
DROP TABLE #CC_TEMP
--DROP TABLE #BASIC_SERVICE_LINE
