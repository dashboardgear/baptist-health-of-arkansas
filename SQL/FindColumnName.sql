/*	FindColumnName.sql
	10/1/13 pjb
	From Pinal Dave at
	 http://blog.sqlauthority.com/2008/08/06/sql-server-query-to-find-column-from-all-tables-of-database/
*/

USE LAWSONDW  --LawsonApps9
GO

SELECT t.name AS table_name,
SCHEMA_NAME(schema_id) AS schema_name,
c.name AS column_name
FROM sys.tables AS t
INNER JOIN sys.columns c ON t.OBJECT_ID = c.OBJECT_ID
WHERE c.name LIKE '%DATE%'
	and t.name LIKE 'AM%'
ORDER BY schema_name, table_name; 