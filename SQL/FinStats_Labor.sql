/*	FinStats_Labor_rev2.sql
*/

DECLARE @begStartDate date
DECLARE @begEndDate date
DECLARE @startDate date
DECLARE @endDate date
DECLARE @begYear int
DECLARE @year int
DECLARE @month int
DECLARE @counter int
DECLARE @maxCount int
DECLARE @YTD char(1)
SET @begYear=2014  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @year=@begYear
SET @month=6  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @begStartDate=CAST(CAST(@year as char(4)) + RIGHT('00' + LTRIM(@month),2) + '01' AS Datetime)
SET @begEndDate = DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))
SET @startDate=@begStartDate
SET @endDate=@begEndDate
SET @counter=1
SET @maxCount=4 
SET @YTD='N'
DECLARE @payDates int

CREATE TABLE #LABOR_HOLD (
	LOCATION_DESCRIPTION varchar(60) null,
	OT_PCT decimal(12,4) null,
	[Paid FTEs] decimal(12,4) null,
	[Worked FTEs] decimal(12,4) null,
	[Avg Hourly Wage] decimal(12,4) null
)

CREATE TABLE #L_STATS (
	PERIOD varchar(12) null,
	START_DATE date null,
	END_DATE date null,
	YTD	char(1) null,
	LOCATION_DESCRIPTION varchar(60) null,
	PAY_GROUP varchar(3) null,
	FTE_COUNT decimal(18,4) null,
	TOT_HOURS decimal(18,2) null
)

WHILE @counter <= @maxCount
BEGIN

IF @counter=2  -- Previous year 
	BEGIN
		SET @year=@year-1
		SET @startDate=CAST(CAST(@year as char(4)) + RIGHT('00' + LTRIM(@month),2) + '01' AS Datetime)
		SET @endDate = DATEADD(DD,-1,(DATEADD(Month,1,@startDate)))
	END
ELSE IF @counter=3  -- Current year YTD
	BEGIN
		SET @year=@year+1
		SET @startDate=CAST(CAST(@year as char(4)) + '01' + '01' AS Datetime)
		SET @endDate=@begEndDate
		SET @YTD='Y'
	END
ELSE IF @counter=4 -- Previous YTD
	BEGIN
		SET @year=@year-1
		SET @startDate=CAST(CAST(@year as char(4)) + '01' + '01' AS Datetime)
		IF ((@year % 4 = 0 AND @year % 100 <> 0) OR @year % 400 = 0) -- Leap Year
			IF @month=2
				SET @endDate=DATEADD(DD,1,(DATEADD(YYYY,1,@begEndDate)))
			ELSE
				SET @endDate=DATEADD(YYYY,-1,@begEndDate)
		ELSE		
				SET @endDate=DATEADD(YYYY,-1,@begEndDate)
	END

SET @counter=@counter+1

--DECLARE @payDates int
SET @payDates=(SELECT COUNT(DISTINCT PER_END_DATE) FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS WHERE PER_END_DATE BETWEEN @startDate AND @endDate)

/**************** LABOR STATS ******************************/
--Labor Statistics
IF OBJECT_ID('tempdb..#glLocations') IS NULL
BEGIN
	CREATE TABLE #glLocations (
		AU_L1 varchar(15),
		AU_L1_NAME varchar(30),
		LOCATION_DESCRIPTION varchar(60) null
	)
INSERT INTO #glLocations
SELECT DISTINCT
	ACCT_UNIT_LEVEL1 AS AU_L1,
	ACCT_UNIT_LEVEL1_NAME AS AU_L1_NAME,
	CASE ACCT_UNIT_LEVEL1
		WHEN 100 THEN 'BHMC PARENT REHABILITATION INSTITUTE'
		WHEN 200 THEN 'BHMC PARENT NORTH LITTLE ROCK'
		WHEN 300 THEN 'BHMC PARENT LITTLE ROCK'
		WHEN 400 THEN 'BHMC PARENT ARKADELPHIA'
		WHEN 500 THEN 'BHMC PARENT STUTTGART'
		WHEN 600 THEN 'BHMC PARENT HEBER SPRINGS'
		WHEN 800 THEN 'BHMC PARENT EXTENDED CARE'
	END AS LOCATION_DESCRIPTION
FROM LAWSONDW.dbo.ACCT_UNIT_STRUCTURE
WHERE 
	ACCT_UNIT_LEVEL1 IN ('100','200','300','400','500','600','800')
ORDER BY ACCT_UNIT_LEVEL1
END
--select * from #glLocations

/**************** FTEs **************************/

;WITH FTE AS (
SELECT 
	GLL.LOCATION_DESCRIPTION,
	PSG.PAY_CLASS AS PAY_GROUP,
	SUM(PRD.HOURS) AS TOT_HOURS
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'PD'  --IN ('PD','PRD','OVT')
		AND PSG.HOURS_FLAG='I'
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate  --BETWEEN '2014-06-01' AND '2014-06-30'
GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS
UNION ALL
SELECT 
	GLL.LOCATION_DESCRIPTION,
	PSG.PAY_CLASS AS PAY_GROUP,
	SUM(PRD.HOURS) AS TOT_HOURS
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'PRD'  --IN ('PD','PRD','OVT')
		AND PSG.HOURS_FLAG='I'
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate  --BETWEEN '2014-06-01' AND '2014-06-30'
GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS
UNION ALL
SELECT 
	GLL.LOCATION_DESCRIPTION,
	PSG.PAY_CLASS AS PAY_GROUP,
	SUM(PRD.HOURS) AS TOT_HOURS
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'OVT'  --IN ('PD','PRD','OVT')
		AND PSG.HOURS_FLAG='I'
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate  --BETWEEN '2014-06-01' AND '2014-06-30'
GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS
UNION ALL
SELECT 
	GLL.LOCATION_DESCRIPTION,
	'TOT' AS PAY_GROUP,
	SUM(PRD.HOURS) AS TOT_HOURS
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS IN ('PD','PRD','OVT')
		AND PSG.HOURS_FLAG='I'
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate  --BETWEEN '2014-06-01' AND '2014-06-30'
GROUP BY GLL.LOCATION_DESCRIPTION
)
SELECT
	LOCATION_DESCRIPTION,
	PAY_GROUP,
	CONVERT(DECIMAL(12,4),ROUND(TOT_HOURS/(80*@payDates),4)) AS FTE_COUNT,
	TOT_HOURS
INTO #FTE_COUNTS
FROM FTE

--SELECT @startDate AS START_DATE, @endDate AS END_DATE, @YTD AS YTD
--SELECT * FROM #FTE_COUNTS
--INSERT INTO #L_STATS
--SELECT 
--	CASE
--		WHEN @startDate='2014-06-01' THEN 'CurMonth'
--		WHEN @startDate='2013-06-01' THEN 'PrevYear'
--		WHEN @startDate='2014-01-01' THEN 'YTD'
--		WHEN @startDate='2013-01-01' THEN 'PrevYTD'
--	END AS PERIOD,
--	@startDate AS START_DATE, @endDate AS END_DATE, @YTD AS YTD,
--	* 
--FROM #FTE_COUNTS


--SELECT * FROM #L_STATS
--DROP TABLE #glLocations
--DROP TABLE #L_STATS

/******************* WAGES *********************/
SELECT
	GLL.LOCATION_DESCRIPTION,
	'OVT' AS PAY_GROUP,
	SUM(PRD.DIST_AMT) AS TOT_AMOUNT
INTO #WAGES
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'OVT'  --IN ('PD','PRD','OVT')
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate 

GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS
UNION ALL
SELECT
	GLL.LOCATION_DESCRIPTION,
	'PD' AS PAY_GROUP,
	SUM(PRD.DIST_AMT) AS TOT_AMOUNT
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'PD'  --IN ('PD','PRD','OVT')
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate 
GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS
UNION ALL
SELECT
	GLL.LOCATION_DESCRIPTION,
	'PRD' AS PAY_GROUP,
	SUM(PRD.DIST_AMT) AS TOT_AMOUNT
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS = 'PRD'  --IN ('PD','PRD','OVT')
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate 
GROUP BY GLL.LOCATION_DESCRIPTION, PSG.PAY_CLASS

UNION ALL
SELECT
	GLL.LOCATION_DESCRIPTION,
	'TOT' AS PAY_GROUP,
	SUM(PRD.DIST_AMT) AS TOT_AMOUNT
FROM LAWSONDWHR.dbo.PR_DISTRIBUTIONS PRD
	INNER JOIN #glLocations GLL on
		GLL.AU_L1=LEFT(PRD.DST_ACCT_UNIT,3)
	INNER JOIN LAWSONDWHR.dbo.LAW_PRPAYCODE PRP ON
		PRD.PCD_SEQ_NBR=PRP.SEQ_NBR
	INNER JOIN LAWSONDWHR.dbo.LAW_PSGRELATE PSG ON
		PSG.PAY_SUM_GRP=PRP.PAY_SUM_GRP
		AND PSG.PAY_CLASS IN ('PD','PRD','OVT')
WHERE PRD.PER_END_DATE BETWEEN @startDate AND @endDate 
GROUP BY
	GLL.LOCATION_DESCRIPTION
	
--select * from #FTE_COUNTS
--select * from #WAGES
/************* Calculations ****************/
SELECT
	LOCATION_DESCRIPTION,
	'Paid FTEs' AS STAT_NAME,
	FTE_COUNT AS STAT_VALUE
INTO #CALCS
FROM #FTE_COUNTS
WHERE PAY_GROUP='PD'
UNION ALL
SELECT
	LOCATION_DESCRIPTION,
	'Worked FTEs' AS STAT_NAME,
	FTE_COUNT AS STAT_VALUE
FROM #FTE_COUNTS
WHERE PAY_GROUP='PRD'
UNION ALL
SELECT
	F.LOCATION_DESCRIPTION,
	'Avg Hourly Wage' AS STAT_NAME,
	CONVERT(DECIMAL(12,4),ROUND(W.TOT_AMOUNT/F.TOT_HOURS,4)) AS STAT_VALUE
FROM #FTE_COUNTS F
	INNER JOIN #WAGES W ON 
		W.LOCATION_DESCRIPTION=F.LOCATION_DESCRIPTION
		AND W.PAY_GROUP=F.PAY_GROUP
WHERE F.PAY_GROUP='PD'

SELECT
	F.LOCATION_DESCRIPTION,
	'Avg Hourly Wage' AS STAT_NAME,
	F.PAY_GROUP,
	F.TOT_HOURS,
	W.PAY_GROUP,
	W.TOT_AMOUNT,
	CONVERT(DECIMAL(12,4),ROUND(W.TOT_AMOUNT/F.TOT_HOURS,4)) AS STAT_VALUE
FROM #FTE_COUNTS F
	INNER JOIN #WAGES W ON 
		W.LOCATION_DESCRIPTION=F.LOCATION_DESCRIPTION
		AND W.PAY_GROUP=F.PAY_GROUP
WHERE F.PAY_GROUP='PD'

CREATE TABLE #OT_CALC (
	LOCATION_DESCRIPTION varchar(60) null,
	OVT decimal(18,2) null,
	PD decimal(18,2) null
)
INSERT INTO #OT_CALC
SELECT *
FROM (SELECT PAY_GROUP, LOCATION_DESCRIPTION, TOT_HOURS FROM #FTE_COUNTS)
AS SOURCE
PIVOT
(
	MAX(TOT_HOURS)
	FOR PAY_GROUP IN ([OVT],[PD])
) AS RESULT	

INSERT INTO #CALCS
SELECT
	LOCATION_DESCRIPTION,
	'OT_PCT' AS STAT_NAME,
	CONVERT(DECIMAL(12,4),ROUND(OVT*100 / PD,4)) AS STAT_VALUE
FROM #OT_CALC

--SELECT *
--INTO #LABOR_CALCS
--FROM #CALCS
--AS SOURCE
--PIVOT
--(
--	MAX(STAT_VALUE)
--	FOR STAT_NAME IN ([OT_PCT],[Paid FTEs],[Worked FTEs],[Avg Hourly Wage])
--) AS RESULT
----------------------------------------

--SELECT
--	LC.LOCATION_DESCRIPTION,
--	'FTE_OPADJ' AS STAT_NAME,
--	--CONVERT(DECIMAL(12,6),ROUND(LC.STAT_VALUE / AP.OP_ADJ,4)) AS STAT_VALUE
--	CONVERT(DECIMAL(12,6),ROUND(LC.STAT_VALUE / APD.ADJ_PT_DAY,4)) AS STAT_VALUE
--INTO #F_CALC
--FROM #CALCS LC
--	INNER JOIN #ADJ_PT_DAY APD ON
--		APD.LOCATION_DESCRIPTION=LC.LOCATION_DESCRIPTION
--	--INNER JOIN #OPADJ AP ON
--	--	AP.LOCATION_DESCRIPTION=LC.LOCATION_DESCRIPTION
--	--INNER JOIN #ADJ_PT_DISCH AD ON
--	--	AD.LOCATION_DESCRIPTION=LC.LOCATION_DESCRIPTION
--WHERE 
--	LC.STAT_NAME='Paid FTEs'
--UNION ALL
--SELECT
--	LC.LOCATION_DESCRIPTION,
--	'FTE_ADJD' AS STAT_NAME,
--	CONVERT(DECIMAL(12,6),ROUND(LC.STAT_VALUE / AD.ADJ_PT_DISCH,4)) AS STAT_VALUE
--FROM #CALCS LC
--	--INNER JOIN #OPADJ AP ON
--	--	AP.LOCATION_DESCRIPTION=LC.LOCATION_DESCRIPTION
--	INNER JOIN #ADJ_PT_DISCH AD ON
--		AD.LOCATION_DESCRIPTION=LC.LOCATION_DESCRIPTION
--WHERE 
--	LC.STAT_NAME='Paid FTEs'


SELECT *
INTO #LABOR_CALCS
FROM #CALCS
AS SOURCE
PIVOT
(
	MAX(STAT_VALUE)
	FOR STAT_NAME IN ([OT_PCT],[Paid FTEs],[Worked FTEs],[Avg Hourly Wage])  --,[FTE_OPADJ],[FTE_ADJD])
) AS RESULT

INSERT INTO #LABOR_HOLD
SELECT * 
FROM #LABOR_CALCS

DROP TABLE #FTE_COUNTS
DROP TABLE #WAGES
DROP TABLE #OT_CALC
DROP TABLE #CALCS
--DROP TABLE #F_CALC
--DROP TABLE #L_STATS
DROP TABLE #LABOR_CALCS

END

select * FROM #LABOR_HOLD

DROP TABLE #glLocations
DROP TABLE #L_STATS
DROP TABLE #LABOR_HOLD

----

--DROP TABLE #FTE_COUNTS
--DROP TABLE #WAGES
--DROP TABLE #OT_CALC
--DROP TABLE #CALCS
--DROP TABLE #F_CALC
--DROP TABLE #ADJ_PT_DAY
--DROP TABLE #ADJ_PT_DISCH
--DROP TABLE #OPADJ
--DROP TABLE #PT_DAY
--DROP TABLE #locTable2



select * FROM #LABOR_CALCS

--DROP TABLE #LABOR_CALCS


