/*	Financial Stat Report Statistics_Store24Months_EXEC.sql
*/

DECLARE @begStartDate date
DECLARE @begEndDate date
DECLARE @begYear int
DECLARE @year int
DECLARE @month int
DECLARE @strMonth varchar(2)
DECLARE @strYear varchar(4)
DECLARE @spCounter int
DECLARE @spMaxCount int
SET @spCounter=1
SET @begYear=2014  --@P_YEAR--<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @month=7  --@P_MONTH<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @year=@begYear
SET @strMonth=RIGHT('00' + LTRIM(CONVERT(varchar(2),@Month)),2)  --ensures leading 0 for single-digit months
SET @strYear=CONVERT(varchar(4),@year)
SET @begStartDate=CONVERT(DATE,(@strYear + RIGHT('00' + LTRIM(@strMonth),2) + '01'))
SET @begEndDate = DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))


IF DATEADD(mm,-24,@begStartDate)<'2013-06-01'
	BEGIN
		SET @begStartDate='2013-06-01'
		SET @spMaxCount=DATEDIFF(month,@begStartDate,@begEndDate)+1
	END
ELSE
	SET @spMaxCount=24
SET @year=YEAR(@begStartDate)

SET @spMaxCount=1
	
WHILE @spCounter<=@spMaxCount
BEGIN

IF @spCounter > 1
	BEGIN
		IF @month=12
		BEGIN
			SET @year=@year+1
			SET @month=1
		END
		ELSE
		SET @month=@month+1
	END

EXEC dbo.sp_BH_MONTHLY_FINANCIAL_STATISTICS_TEST @year, @month 

SET @spCounter=@spCounter+1

END