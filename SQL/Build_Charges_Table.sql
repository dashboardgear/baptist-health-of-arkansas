/*	Build_Charges_Table_rev6.sql
	5/8/14
	Create reporting table BH_RPT_CHARGES from extract source tables
	-6/3/14 Create temp table _before_ truncating target table
	-6/17/14 Exclude certain Default Revenue Codes
	-6/24/14 When building BH_CHARGE_ITEM use all characters of the EXTERNAL_PROCEDURE_CODE, 
		not just right 6
	-6/24/14 Reverse exclusion of certain Default Revenue Codes (per Brock)
	-6/26/14 Change PTR.INSURANCE_GROUP_NAME to PTR.INSURANCE_GROUP to match BH_RPT_PATIENTS
*/

SELECT 
	CHG.ACCOUNT_ID
	,CHG.LOCATION
	,LOC.LOCATION_DESCRIPTION
	,CHG.REV_CODE
	,CHG.REV_CODE_DESCRIPTION
	,CHG.DEFAULT_REV_CODE
	,CHG.DEFAULT_REV_CODE_DESC
	,CHG.EXTERNAL_PROCEDURE_CODE
	,CHG.EXTERNAL_PROCEDURE_CODE_DESC
	,CHG.COST_CENTER_CODE + '-' + CHG.EXTERNAL_PROCEDURE_CODE + '-' +
		RIGHT('000000'+CHG.ORSOS_CODE,6) + '-' + RIGHT('00000000000'+ISNULL(CHG.NATIONAL_DRUG_CODE,'00000000000'),11) 
		AS BH_CHARGE_ITEM
	,CHG.SERVICE_DATE
	,DATEDIFF (dd,PTR.BILLING_ADMIT_DATE,CHG.SERVICE_DATE)+1 AS DAY_OF_STAY
	,CHG.POST_DATE
	,CHG.COST_CENTER_CODE
	,CHG.COST_CENTER_DESCRIPTION
	,CHG.ORSOS_CODE
	,CHG.NATIONAL_DRUG_CODE
	,CHG.CPT_CODE
	,CPT.SHORT_DESCRIPTOR AS CPT_CODE_DESCRIPTION
	,CHG.AMOUNT
	,CHG.COST_QUANTITY
	,CHG.QUANTITY
	,CHG.DEFAULT_PRICE
	,PTR.BILLING_ADMIT_DATE
	,PTR.DISCHARGE_DATETIME
	,PTR.DRG
	,PTR.DRG_DESCRIPTION
	,PTR.ADMITTING_PHYSICIAN_CODE
	,PTR.ADMITTING_PHYSICIAN_NAME
	,PTR.PRIMARY_FINANCIAL_CLASS
	,PTR.PRIMARY_FINANCIAL_CLASS_DESC
	,PTR.PRIMARY_INSURANCE_PLAN
	,PTR.PRIMARY_INSURANCE_PLAN_DESC
	,PTR.ACCT_BASE_CLASS
	,PTR.INSURANCE_GROUP
    ,PTR.SERVICE_LINE_BASIC
    ,PTR.SERVICE_LINE_DETAIL
INTO #c1
FROM BH_EPIC_CHARGES CHG
	INNER JOIN BH_EPIC_LOCATIONS LOC ON
		LOC.LOCATION=CHG.LOCATION
	INNER JOIN BH_RPT_PATIENTS PTR ON
		PTR.ACCOUNT_ID=CHG.ACCOUNT_ID
	LEFT OUTER JOIN BH_CPT_REFERENCE CPT ON  --Always one and only one match?
		CPT.HCPCS_CODE=CHG.CPT_CODE
		AND CHG.SERVICE_DATE BETWEEN CPT.EFFECTIVE_BEGIN AND CPT.EFFECTIVE_END


TRUNCATE TABLE BH_RPT_CHARGES
INSERT INTO BH_RPT_CHARGES
SELECT * FROM #c1

DROP TABLE #c1
