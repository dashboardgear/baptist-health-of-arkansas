--Test 24-Period Date Increments.sql

DECLARE @begStartDate date
DECLARE @begEndDate date
DECLARE @startDate date
DECLARE @endDate date
DECLARE @begYear int
DECLARE @year int
DECLARE @month int
DECLARE @glPeriod int
DECLARE @strMonth varchar(2)
DECLARE @strYear varchar(4)
DECLARE @counter int
DECLARE @maxCount int
DECLARE @YTD char(1)
DECLARE @payDates int

SET @begYear=2014  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @month=7  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @year=@begYear
SET @strMonth=RIGHT('00' + LTRIM(CONVERT(varchar(2),@Month)),2)  --ensures leading 0 for single-digit months
SET @strYear=CONVERT(varchar(4),@year)
--SET @begStartDate=CAST(CAST(@year as char(4)) + RIGHT('00' + LTRIM(@month),2) + '01' AS Datetime)
SET @begStartDate=CONVERT(DATE,(@strYear + RIGHT('00' + LTRIM(@strMonth),2) + '01'))
SET @begEndDate = DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))
SET @startDate=@begStartDate
SET @endDate=@begEndDate
SET @counter=1
SET @maxCount=24
SET @YTD='N'
SET @glPeriod=@month

--SELECT 
--	@begStartDate AS BEG_STARTDATE,
--	@begEndDate AS BEG_ENDATE,
--	@startDate as STARTDATE,
--	@endDate AS ENDATE

CREATE TABLE #dateTable   (
	COUNTER_NBR int null,
	BEG_START_DATE date null,
	MAX_COUNT int null,
	STARTDATE date null,
	ENDDATE date null,
	GLPERIOD int null
)

IF DATEADD(mm,-24,@begStartDate)<'2013-06-01'
	BEGIN
		SET @begStartDate='2013-06-01'
		SET @maxCount=DATEDIFF(month,@begStartDate,@begEndDate)+1
	END
ELSE
	SET @begStartDate=DATEADD(month,-24,@begStartDate)
	
SET @startDate=@begStartDate
SET @endDate = DATEADD(DD,-1,(DATEADD(Month,1,@startDate)))

WHILE @counter <= @maxCount
BEGIN

IF @counter > 1
	BEGIN
		SET @startDate=DATEADD(month,1,@startDate)
		SET @endDate = DATEADD(DD,-1,(DATEADD(Month,1,@startDate)))
	END

--SET @counter=@counter+1 Moved down to before END
SET @year=YEAR(@startDate)
IF @glPeriod=12
	SET @glPeriod=1
ELSE
	SET @glPeriod=@glPeriod+1
SET @month=@glPeriod

INSERT INTO #dateTable
SELECT
	@counter AS COUNTER_NBR,
	@begStartDate AS BEG_START_DATE,
	@maxCount AS MAX_COUNT,
	@startDate AS STARTDATE,
	@endDate AS ENDDATE,
	@glPeriod AS GLPERIOD

SET @counter=@counter+1

END

SELECT * FROM #dateTable

DROP TABLE #dateTable