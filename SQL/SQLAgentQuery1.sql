/*	SQLAgentQuery1.sql
*/

DECLARE @year int
DECLARE @month int

SET @month=
	CASE
		WHEN MONTH(GETDATE())=1 THEN 12
		ELSE MONTH(GETDATE())-1
	END 
SET @year=
	CASE
		WHEN MONTH(GETDATE())=1 THEN YEAR(GETDATE())-1
		ELSE YEAR(GETDATE())
	END
	
SELECT
	@year as YEAR_NUM,
	@month as MONTH_NUM


EXEC sp_BH_FINANCIAL_STATISTICS_24 @year, @month