/*	Get Temp Table Data Types Example.sql
	7/9/14 pb
*/

SELECT *
INTO #T1
FROM ACFACTS

;WITH TYPES AS (
SELECT
	name,
	CASE user_type_id
		WHEN 127 THEN 'bigint'
		WHEN 173 THEN 'binary'
		WHEN 104 THEN 'bit'
		WHEN 175 THEN 'char'
		WHEN 61 THEN 'datetime'
		WHEN 106 THEN 'decimal'
		WHEN 62 THEN 'float'
		WHEN 34 THEN 'image'
		WHEN 56 THEN 'int'
		WHEN 60 THEN 'money'
		WHEN 239 THEN 'nchar'
		WHEN 99 THEN 'ntext'
		WHEN 108 THEN 'numeric'
		WHEN 231 THEN 'nvarchar'
		WHEN 59 THEN 'real'
		WHEN 58 THEN 'smalldatetime'
		WHEN 52 THEN 'smallint'
		WHEN 122 THEN 'smallmoney'
		WHEN 98 THEN 'sql_variant'
		WHEN 256 THEN 'sysname'
		WHEN 35 THEN 'text'
		WHEN 189 THEN 'timestamp'
		WHEN 48 THEN 'tinyint'
		WHEN 36 THEN 'uniqueidentifier'
		WHEN 165 THEN 'varbinary'
		WHEN 167 THEN 'varchar'
		WHEN 241 THEN 'xml'
		ELSE '??'
	END AS DATA_TYPE,
	max_length,
	precision,
	scale,
	collation_name,
	is_nullable
FROM tempdb.sys.columns 
WHERE [object_id] = OBJECT_ID(N'tempdb..#T1')
)
SELECT
	name,
	CASE
		WHEN DATA_TYPE = 'varchar' THEN DATA_TYPE+'('+CONVERT(varchar(3),max_length)+')'
		WHEN DATA_TYPE = 'decimal' OR DATA_TYPE='numeric' 
			THEN DATA_TYPE+'('+CONVERT(varchar(3),precision)+','+CONVERT(varchar(3),scale)+')'
		ELSE DATA_TYPE
	END AS DATA_TYPE ,
	is_nullable,
	collation_name
	

FROM TYPES

DROP TABLE #T1