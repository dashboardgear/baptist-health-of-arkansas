/*	SetIncludeFlag.sql
	5/1/2014
	Sets INCLUDE_FLAG to 'N' if
		Total_Charges = 0 OR
		Patient_Status = 'Combined' OR
		Admit_Date, IP_Admit_Date and Disch_Date are all blank
	5/22/14 - Add REPLACE for char(13) in last imported field
*/
UPDATE    dbo.BH_EPIC_PATIENTS_TEMP
SET       INCLUDE_FLAG = (CASE WHEN (TOTAL_CHARGES = 0 OR
                      PATIENT_STATUS = 'Combined' OR
                      (ADMIT_DATE IS NULL AND IP_ADMIT_DATE IS NULL AND DISCHARGE_DATE IS NULL)) THEN 'N' ELSE 'Y' END),
          GUARANTOR_TYPE_DESC=REPLACE(GUARANTOR_TYPE_DESC,CHAR(13),'');