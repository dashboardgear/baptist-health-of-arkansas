/*	DrillRpt_IP DC Drill Down1-SL.sql
	8/6/2014 - Dashboard Gear
	IP Drill-down from Monthly Financial Statistics - Detail Service Line version
*/

DECLARE @begStartDate date
DECLARE @startDate date
DECLARE @endDate date
DECLARE @month int
DECLARE @year int
DECLARE @strMonth varchar(2)
DECLARE @strYear varchar(4)
SET @year=2014 --(@P_YEAR parameter)
SET @month=7  --(@P_MONTH parameter)
SET @strMonth=CONVERT(varchar(2),@month)
SET @strYear=CONVERT(varchar(4),@year)

SET @begStartDate=CONVERT(DATE,(@strYear + RIGHT('00' + LTRIM(@strMonth),2) + '01'))
SET @startDate = DATEADD(mm,-24,@begStartDate)	
SET @endDate=DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))

;WITH DATA AS (
SELECT 
	'Discharges' StatType,
	LOCATION_DESCRIPTION,
	CONVERT(DATE,DISCHARGE_DATETIME) AS ADM_DIS_DATE,
	CONVERT(varchar(4),YEAR(CONVERT(DATE,DISCHARGE_DATETIME)))+'-'+RIGHT('00' + LTRIM(CONVERT(varchar(2),MONTH(CONVERT(DATE,DISCHARGE_DATETIME)))),2) AS PERIOD,
	--DATENAME(mm,SERVICE_DATE) AS [MONTH],
	YEAR(CONVERT(DATE,DISCHARGE_DATETIME)) AS [YEAR],
	MONTH(CONVERT(DATE,DISCHARGE_DATETIME)) AS MONTH_NUM,
	SERVICE_LINE_BASIC,
	SERVICE_LINE_DETAIL,
	CASE_COUNT
FROM BH_RPT_PATIENTS
WHERE
	ACCT_BASE_CLASS='IP'
	AND CONVERT(DATE,DISCHARGE_DATETIME) BETWEEN @startDate AND @endDate
UNION ALL
SELECT 
	'Admissions', 
	LOCATION_DESCRIPTION,
	CONVERT(DATE,ADMIT_DATETIME) ADMIT_DATETIME,
	CONVERT(varchar(4),YEAR(CONVERT(DATE,ADMIT_DATETIME)))+'-'+RIGHT('00' + LTRIM(CONVERT(varchar(2),MONTH(CONVERT(DATE,ADMIT_DATETIME)))),2) AS PERIOD,
	YEAR(CONVERT(DATE,ADMIT_DATETIME)) AS [YEAR],
	MONTH(CONVERT(DATE,ADMIT_DATETIME)) AS MONTH_NUM,
	SERVICE_LINE_BASIC,
	SERVICE_LINE_DETAIL,
	CASE_COUNT
FROM BH_RPT_PATIENTS
WHERE
	ACCT_BASE_CLASS='IP'
	AND  CONVERT(DATE,ADMIT_DATETIME) BETWEEN @startDate AND @endDate
)
SELECT
	StatType,
	LOCATION_DESCRIPTION,
	[YEAR],
	MONTH_NUM,
	PERIOD,
	SERVICE_LINE_BASIC,
	SERVICE_LINE_DETAIL,
	COUNT(CASE_COUNT) AS DISCHARGE_COUNT
FROM DATA
GROUP BY StatType, LOCATION_DESCRIPTION, SERVICE_LINE_BASIC, SERVICE_LINE_DETAIL, [YEAR], MONTH_NUM, PERIOD 
HAVING SERVICE_LINE_BASIC IN 
	(SELECT SERVICE_LINE_BASIC FROM BH_SL_REPORT_BASIC WHERE INCLUDE='Y')
ORDER BY 
	LOCATION_DESCRIPTION,[YEAR], MONTH_NUM, SERVICE_LINE_BASIC, SERVICE_LINE_DETAIL,  StatType

	
