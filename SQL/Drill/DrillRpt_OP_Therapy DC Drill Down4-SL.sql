/*	DrillRpt_OP_Therapy DC Drill Down4-SL_rev1.sql
	8/7/2014 - Dashboard Gear
	OP Therapy Visits
	Paul's change test
*/

DECLARE @begStartDate date
DECLARE @startDate date
DECLARE @endDate date
DECLARE @month int
DECLARE @year int
DECLARE @strMonth varchar(2)
DECLARE @strYear varchar(4)
SET @year=2014 --(@P_YEAR parameter)
SET @month=7  --(@P_MONTH parameter)
SET @strMonth=CONVERT(varchar(2),@month)
SET @strYear=CONVERT(varchar(4),@year)

SET @begStartDate=CONVERT(DATE,(@strYear + RIGHT('00' + LTRIM(@strMonth),2) + '01'))
SET @startDate = DATEADD(mm,-24,@begStartDate)	
SET @endDate=DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))

/*********** OP Rehab Visits  **************/
--(Base code from Robert Furrey)--
-- Get Patient Accounts
SELECT
	ACCOUNT_ID
	,SUM(QUANTITY) AS TOTAL_QUANTITY
INTO #VISITSBYACCOUNT
FROM BH_RPT_CHARGES
WHERE
	SERVICE_DATE BETWEEN @startDate AND @endDate
	AND SERVICE_LINE_BASIC = 'PT,OT,ST' AND REV_CODE BETWEEN '420' AND '449'
GROUP BY
	ACCOUNT_ID
HAVING SUM(AMOUNT) > 0 AND SUM(QUANTITY) > 0
ORDER BY
	ACCOUNT_ID
	
--Get Daily Charges--
;WITH DATA AS (
SELECT
	LOCATION_DESCRIPTION,
	SERVICE_DATE,
	CONVERT(varchar(4),YEAR(CONVERT(DATE,DISCHARGE_DATETIME)))+'-'+RIGHT('00' + LTRIM(CONVERT(varchar(2),MONTH(CONVERT(DATE,DISCHARGE_DATETIME)))),2) AS PERIOD,
	YEAR(SERVICE_DATE) AS [YEAR],
	MONTH(SERVICE_DATE) AS MONTH_NUM,
	SERVICE_LINE_BASIC,
	CHG.COST_CENTER_CODE+' - '+COST_CENTER_DESCRIPTION AS COST_CENTER_DESCRIPTION,
	QUANTITY
FROM BH_RPT_CHARGES CHG
	--INNER JOIN #VISITSBYACCOUNT VBA ON  --ONLY PULLS CASES WITH >0 CHARGES AND QUANTITY
	--	CHG.ACCOUNT_ID = VBA.ACCOUNT_ID
WHERE
	SERVICE_DATE BETWEEN @startDate AND @endDate
	AND
	(SERVICE_LINE_BASIC = 'PT,OT,ST' AND REV_CODE BETWEEN '420' AND '449')
	AND ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM #VISITSBYACCOUNT)  --ONLY PULLS CASES WITH >0 CHARGES AND QUANTITY
)
SELECT
	LOCATION_DESCRIPTION,
	[YEAR],
	MONTH_NUM,
	PERIOD,
	SERVICE_LINE_BASIC,
	COST_CENTER_DESCRIPTION,
	SUM(QUANTITY) AS OP_THERAPY_VISITS

FROM DATA
GROUP BY LOCATION_DESCRIPTION, COST_CENTER_DESCRIPTION, SERVICE_LINE_BASIC, [YEAR], MONTH_NUM, PERIOD
ORDER BY 
	LOCATION_DESCRIPTION, [YEAR], MONTH_NUM, COST_CENTER_DESCRIPTION, SERVICE_LINE_BASIC

DROP TABLE #VISITSBYACCOUNT
