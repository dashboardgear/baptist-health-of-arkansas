/*	OP Adjustment Factor_Robert_rev1.sql
	7/6/114
	From Robert Furrey's OP Adjustment Factor Query
*/
DECLARE @begin date
DECLARE @end date
SET @begin='2014-05-01'
--SET @end= '2014-05-31';
SET @end = DATEADD(DD,-1,(DATEADD(Month,1,@begin)));

--Creates a table that sums revenue by base class and organzation for a given time period
;WITH Totals AS (
select
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS,
	--convert(decimal(20,4),sum(AMOUNT)) As 'Total_Revenue'
	SUM(AMOUNT) AS 'Total_Revenue'
from BH_RPT_CHARGES
where
	SERVICE_DATE BETWEEN @begin and @end  --'2014-05-01' and '2014-05-31' -- 
group by
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS
)	
--Create a pivot table that has 1 record for reach organzation containint all base_types
select
	LOCATION,
	LOCATION_DESCRIPTION,
	ISNULL(ED,0) AS ED,
	ISNULL(IP,0) AS IP,
	ISNULL(OP,0) AS OP

into #OPAdjFactor
FROM TOTALS
PIVOT
(
    sum(Total_Revenue)
FOR
ACCT_BASE_CLASS
    IN ( ED, IP, OP)
) AS WHATEVER

--Perform the calculation
select
	LOCATION,
	LOCATION_DESCRIPTION,
	(ED + OP + IP) / IP as OP_Adjustment_Factor
-- #OPAdjFactor2
from #OPAdjFactor

drop table #opadjfactor
