/*	OP Ajustment Factor TEST.sql
*/

DECLARE @begStartDate date
DECLARE @begEndDate date
DECLARE @startDate date
DECLARE @endDate date
DECLARE @begYear int
DECLARE @year int
DECLARE @month int
DECLARE @counter int
DECLARE @maxCount int
DECLARE @YTD char(1)
SET @begYear=2014  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @year=@begYear
SET @month=6  --<<<<<<<<<<<<<<<<<<<<<<<<<<< Parameter Here <<<<<<<<<<<<<<<<<<<<<<<<<<<
SET @begStartDate=CAST(CAST(@year as char(4)) + RIGHT('00' + LTRIM(@month),2) + '01' AS Datetime)
SET @begEndDate = DATEADD(DD,-1,(DATEADD(Month,1,@begStartDate)))
SET @startDate=@begStartDate
SET @endDate=@begEndDate
SET @counter=1
SET @maxCount=4 
SET @YTD='N'

WHILE @counter <= @maxCount
BEGIN

IF @counter=2  -- Previous year 
	BEGIN
		SET @year=@year-1
		SET @startDate=CAST(CAST(@year as char(4)) + RIGHT('00' + LTRIM(@month),2) + '01' AS Datetime)
		SET @endDate = DATEADD(DD,-1,(DATEADD(Month,1,@startDate)))
	END
ELSE IF @counter=3  -- Current year YTD
	BEGIN
		SET @year=@year+1
		SET @startDate=CAST(CAST(@year as char(4)) + '01' + '01' AS Datetime)
		SET @endDate=@begEndDate
		SET @YTD='Y'
	END
ELSE IF @counter=4 -- Previous YTD
	BEGIN
		SET @year=@year-1
		SET @startDate=CAST(CAST(@year as char(4)) + '01' + '01' AS Datetime)
		IF ((@year % 4 = 0 AND @year % 100 <> 0) OR @year % 400 = 0) -- Leap Year
			IF @month=2
				SET @endDate=DATEADD(DD,1,(DATEADD(YYYY,1,@begEndDate)))
			ELSE
				SET @endDate=DATEADD(YYYY,-1,@begEndDate)
		ELSE		
				SET @endDate=DATEADD(YYYY,-1,@begEndDate)
	END

SET @counter=@counter+1

;WITH Totals AS (
SELECT
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS,
	SUM(AMOUNT) AS 'Total_Revenue'
FROM BH_RPT_CHARGES
WHERE
	SERVICE_DATE BETWEEN @startDate and @endDate   --'2014-01-01' AND '2014-06-30'  --@endDate  --
GROUP BY
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS
)	
--Create a pivot table that has 1 record for reach organzation containing all base_types
SELECT
	LOCATION,
	LOCATION_DESCRIPTION,
	ISNULL(ED,0) AS ED,
	ISNULL(IP,0) AS IP,
	ISNULL(OP,0) AS OP
INTO #OPAdjFactor
FROM TOTALS
PIVOT
(
    SUM(Total_Revenue)
FOR
ACCT_BASE_CLASS
    IN ( ED, IP, OP)
) AS WHATEVER

--select * from #OPAdjFactor
--DROP TABLE #OPAdjFactor

--END

--Perform the calculation
SELECT
	@counter AS Counter,
	LOCATION_DESCRIPTION,
	(ED + OP + IP) / IP as OP_ADJ
INTO #OPADJ
FROM #OPAdjFactor

DROP TABLE #OPAdjFactor
SELECT * FROM #OPADJ

DROP TABLE #OPADJ

END

/******************* Query for separate ED, IP, OP totals **************/
SELECT
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS,
	SUM(AMOUNT) AS 'Total_Revenue'
FROM BH_RPT_CHARGES
WHERE
	SERVICE_DATE BETWEEN '2014-05-01' AND '2014-05-31'  --@endDate  --
GROUP BY
	LOCATION,
	LOCATION_DESCRIPTION,
	ACCT_BASE_CLASS
ORDER BY LOCATION_DESCRIPTION, ACCT_BASE_CLASS