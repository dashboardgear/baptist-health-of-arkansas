DECLARE @begin date;
DECLARE @end date;
--DECLARE @stattype string;
SET @begin='2014-01-01'
SET @end='2014-06-30'
--SET @stattype='Operational'




/*-------PIS Discharges-------*/
SELECT
	ACCOUNT_ID
	,BH_RPT_PATIENTS.DRG
	,CAST(DISCHARGE_DATETIME AS DATE) AS 'DISCHARGE_DATE'
	,LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,CASE
		WHEN LOCATION = '10201' THEN '1007070'
		WHEN LOCATION = '10200' THEN '2007070'
		WHEN LOCATION = '10199' THEN '3007070'
		WHEN LOCATION = '10202' THEN '4007070'
		WHEN LOCATION = '10204' THEN '5007070'
		WHEN LOCATION = '10206' THEN '5207070'
		WHEN LOCATION = '10203' THEN '6007070'
		WHEN LOCATION = '10205' THEN '8007070'
		ELSE '9007070'
		END AS COST_CENTER_CODE
	,SUM(BH_DRG_REFERENCE.PHARM_INTENSITY_WT) AS TOTAL_QUANTITY
	,SUM(BH_DRG_REFERENCE.PHARM_INTENSITY_WT) AS TOTAL_WEIGHT
	,'PIS Discharges' AS WEIGHT_TYPE_COSTCENTER
INTO #PISDISCHARGES
FROM BH_RPT_PATIENTS
	INNER JOIN BH_DRG_REFERENCE ON
		BH_RPT_PATIENTS.DRG = BH_DRG_REFERENCE.DRG
		AND BH_RPT_PATIENTS.DISCHARGE_DATETIME BETWEEN BH_DRG_REFERENCE.EFFECTIVE_BEGIN AND BH_DRG_REFERENCE.EFFECTIVE_END
WHERE
	CAST(DISCHARGE_DATETIME AS DATE) BETWEEN @begin and @end
	AND ACCT_BASE_CLASS = 'IP'
	AND DISCHARGE_DATETIME IS NOT NULL
GROUP BY
	ACCOUNT_ID
	,BH_RPT_PATIENTS.DRG
	,CAST(DISCHARGE_DATETIME AS DATE)
	,LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL




/*-------PATIENTS-------*/

SELECT
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,COUNT(DISTINCT(ACCOUNT_ID)) AS TOTAL_QUANTITY  --IS THE TOTAL CASES
	,COUNT(DISTINCT(ACCOUNT_ID)) AS TOTAL_WEIGHT  --IS THE TOTAL CASES
	,'Patients' AS WEIGHT_TYPE_COSTCENTER
INTO #PATIENTS
FROM BH_RPT_CHARGES
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		BH_RPT_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
WHERE
	SERVICE_DATE BETWEEN @begin AND @end
GROUP BY
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
ORDER BY
	COST_CENTER_CODE

--SELECT * FROM #PATIENTS

--DROP TABLE #PATIENTS





/*-------CASES (BASE COUNTS)-------*/

SELECT
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE
	,BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC
	,SUM(QUANTITY) AS TOTAL_QUANTITY
	,SUM(QUANTITY) AS TOTAL_WEIGHT
	,'Cases' AS WEIGHT_TYPE_COSTCENTER
INTO #CASES
FROM BH_RPT_CHARGES
	INNER JOIN BH_STAT_CHARGE_WEIGHTS ON
		BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE = BH_STAT_CHARGE_WEIGHTS.EXTERNAL_PROCEDURE_CODE
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		BH_RPT_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
WHERE
	SERVICE_DATE BETWEEN @begin AND @end
	--SERVICE_DATE BETWEEN '2014-01-01' AND '2014-06-30'
	AND ((BH_STAT_CHARGE_WEIGHTS.WEIGHT_TYPE IN ('SURGERY MINUTES', 'CASES') AND BH_STAT_CHARGE_WEIGHTS.WEIGHT = '30')
	OR (BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE='36001161' AND (BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC LIKE '%BASE%') OR (BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC LIKE 'HC O.R. PROC ORSOS'))
	OR (BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE='36001171' AND BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC LIKE '%BASE%'))
GROUP BY
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE
	,BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC

--SELECT * FROM #CASES

--DROP TABLE #CASES





/*-------VISITS-------*/

SELECT
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,SERVICE_DATE
	,COUNT(DISTINCT(ACCOUNT_ID)) AS 'Visits'
INTO #VISITSBYDAY
FROM BH_RPT_CHARGES
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		BH_RPT_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
	JOIN BH_EPIC_LOCATIONS ON
		BH_RPT_CHARGES.LOCATION = BH_EPIC_LOCATIONS.LOCATION
WHERE
	SERVICE_DATE BETWEEN @begin AND @end
GROUP BY
	BH_RPT_CHARGES.LOCATION
	,BH_RPT_CHARGES.LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,SERVICE_DATE
ORDER BY 
	COST_CENTER_CODE

SELECT
	LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,COST_CENTER_CODE
	,COST_CENTER_DESC
	,SUM(VISITS) AS TOTAL_QUANTITY
	,SUM(VISITS) AS TOTAL_WEIGHT
	,'Visits' AS WEIGHT_TYPE_COSTCENTER
INTO #VISITS
FROM #VISITSBYDAY
GROUP BY
	LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,COST_CENTER_CODE
	,COST_CENTER_DESC
ORDER BY
	COST_CENTER_CODE

--SELECT * FROM #VISITS

--DROP TABLE #VISITS

DROP TABLE #VISITSBYDAY  --GO AHEAD AND DROP THIS TABLE WHEN MAKING THE VISITS TABLE





/*-----CHARGE BASED STATS-----*/

SELECT
	BH_RPT_CHARGES.LOCATION
	,BH_EPIC_LOCATIONS.LOCATION_DESCRIPTION
	,BH_RPT_CHARGES.ACCT_BASE_CLASS
	,BH_RPT_CHARGES.SERVICE_LINE_BASIC
	,BH_RPT_CHARGES.SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	--,BH_RPT_CHARGES.SERVICE_DATE
	--,BH_RPT_CHARGES.POST_DATE
	,SUM(BH_RPT_CHARGES.QUANTITY) AS TOTAL_QUANTITY
	,SUM(CASE
		WHEN BH_STAT_CHARGE_WEIGHTS.WEIGHT_TYPE='SURGERY MINUTES' AND (BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE='36001161' OR BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE='36001171') THEN
			CASE
				WHEN BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC LIKE '%BASE%' THEN 30 * BH_RPT_CHARGES.QUANTITY
				WHEN BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE_DESC LIKE '%INCR%' THEN 1 * BH_RPT_CHARGES.QUANTITY
				ELSE BH_STAT_CHARGE_WEIGHTS.WEIGHT * BH_RPT_CHARGES.QUANTITY
			END
		ELSE BH_STAT_CHARGE_WEIGHTS.WEIGHT * BH_RPT_CHARGES.QUANTITY
		END) AS TOTAL_WEIGHT
	,BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE AS WEIGHT_TYPE_COSTCENTER
INTO #STATS
FROM BH_RPT_CHARGES
	JOIN BH_STAT_COSTCENTER_TYPES ON
		BH_RPT_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
	JOIN BH_STAT_CHARGE_WEIGHTS ON
		BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE = BH_STAT_CHARGE_WEIGHTS.WEIGHT_TYPE
		AND BH_RPT_CHARGES.EXTERNAL_PROCEDURE_CODE = BH_STAT_CHARGE_WEIGHTS.EXTERNAL_PROCEDURE_CODE
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		BH_RPT_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
	JOIN BH_EPIC_LOCATIONS ON
		BH_RPT_CHARGES.LOCATION = BH_EPIC_LOCATIONS.LOCATION
WHERE
	BH_STAT_COSTCENTER_TYPES.STATISTIC_TYPE = 'OPERATIONAL'
	AND BH_RPT_CHARGES.SERVICE_DATE BETWEEN @begin AND @end
	--AND BH_RPT_CHARGES.SERVICE_DATE BETWEEN '2014-01-01' AND '2014-06-30'
GROUP BY
	BH_RPT_CHARGES.LOCATION
	,BH_EPIC_LOCATIONS.LOCATION_DESCRIPTION
	,BH_RPT_CHARGES.ACCT_BASE_CLASS
	,BH_RPT_CHARGES.SERVICE_LINE_BASIC
	,BH_RPT_CHARGES.SERVICE_LINE_DETAIL
	,BH_RPT_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	--,BH_RPT_CHARGES.SERVICE_DATE
	--,BH_RPT_CHARGES.POST_DATE
	,BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE
ORDER BY
	BH_RPT_CHARGES.COST_CENTER_CODE





/*-----ADD PATIENTS DATA TO THE #STATS TABLE-----*/

SELECT
	#PATIENTS.LOCATION
	,#PATIENTS.LOCATION_DESCRIPTION
	,#PATIENTS.ACCT_BASE_CLASS
	,#PATIENTS.SERVICE_LINE_BASIC
	,#PATIENTS.SERVICE_LINE_DETAIL
	,#PATIENTS.COST_CENTER_CODE
	,#PATIENTS.COST_CENTER_DESC
	,#PATIENTS.TOTAL_QUANTITY
	,#PATIENTS.TOTAL_WEIGHT
	,#PATIENTS.WEIGHT_TYPE_COSTCENTER
INTO #TEMPADD
FROM #PATIENTS
	JOIN BH_STAT_COSTCENTER_TYPES ON
		#PATIENTS.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
WHERE
	BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE='PATIENTS'
	AND BH_STAT_COSTCENTER_TYPES.STATISTIC_TYPE = 'OPERATIONAL'

INSERT INTO #STATS
SELECT * FROM #TEMPADD

DROP TABLE #TEMPADD





/*-----ADD CASES (BASE COUNTS) DATA TO THE #STATS TABLE-----*/

SELECT
	#CASES.LOCATION
	,#CASES.LOCATION_DESCRIPTION
	,#CASES.ACCT_BASE_CLASS
	,#CASES.SERVICE_LINE_BASIC
	,#CASES.SERVICE_LINE_DETAIL
	,#CASES.COST_CENTER_CODE
	,#CASES.COST_CENTER_DESC
	,#CASES.TOTAL_QUANTITY
	,#CASES.TOTAL_WEIGHT
	,#CASES.WEIGHT_TYPE_COSTCENTER
INTO #TEMPADD3
FROM #CASES
	JOIN BH_STAT_COSTCENTER_TYPES ON
		#CASES.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
WHERE
	BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE='CASES'
	AND BH_STAT_COSTCENTER_TYPES.STATISTIC_TYPE = 'OPERATIONAL'

INSERT INTO #STATS
SELECT * FROM #TEMPADD3

DROP TABLE #TEMPADD3





/*-----ADD VISITS DATA TO THE #STATS TABLE-----*/

SELECT
	#VISITS.LOCATION
	,#VISITS.LOCATION_DESCRIPTION
	,#VISITS.ACCT_BASE_CLASS
	,#VISITS.SERVICE_LINE_BASIC
	,#VISITS.SERVICE_LINE_DETAIL
	,#VISITS.COST_CENTER_CODE
	,#VISITS.COST_CENTER_DESC
	,#VISITS.TOTAL_QUANTITY
	,#VISITS.TOTAL_WEIGHT
	,#VISITS.WEIGHT_TYPE_COSTCENTER
INTO #TEMPADD2
FROM #VISITS
	JOIN BH_STAT_COSTCENTER_TYPES ON
		#VISITS.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
WHERE
	BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE='VISITS'
	AND BH_STAT_COSTCENTER_TYPES.STATISTIC_TYPE = 'OPERATIONAL'

INSERT INTO #STATS
SELECT * FROM #TEMPADD2

DROP TABLE #TEMPADD2




/*-----ADD PIS DISCHARGES-----*/
INSERT INTO #STATS
SELECT
	LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,#PISDISCHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,SUM(TOTAL_QUANTITY) AS TOTAL_QUANTITY
	,SUM(TOTAL_WEIGHT) AS TOTAL_WEIGHT
	,WEIGHT_TYPE_COSTCENTER
FROM #PISDISCHARGES
	JOIN BH_STAT_COSTCENTER_TYPES ON
		#PISDISCHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		#PISDISCHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
WHERE
	BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE='PIS Discharges'
	AND BH_STAT_COSTCENTER_TYPES.STATISTIC_TYPE = 'OPERATIONAL'
GROUP BY
	LOCATION
	,LOCATION_DESCRIPTION
	,ACCT_BASE_CLASS
	,SERVICE_LINE_BASIC
	,SERVICE_LINE_DETAIL
	,#PISDISCHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,WEIGHT_TYPE_COSTCENTER





/*-----ADD HAND KEYED STATS-----*/

INSERT INTO #STATS
SELECT
	CASE
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '100%' THEN 10201
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '200%' THEN 10200
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '300%' THEN 10199
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '400%' THEN 10202
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '500%' THEN 10204
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '520%' THEN 10206
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '600%' THEN 10203
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '800%' THEN 10205
		ELSE 10000
		END AS LOCATION,
	CASE
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '100%' THEN 'BHMC PARENT REHABILITATION INSTITUTE'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '200%' THEN 'BHMC PARENT NORTH LITTLE ROCK'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '300%' THEN 'BHMC PARENT LITTLE ROCK'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '400%' THEN 'BHMC PARENT ARKADELPHIA'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '500%' THEN 'BHMC PARENT STUTTGART'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '520%' THEN 'BHMC PARENT HOT SPRING COUNTY'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '600%' THEN 'BHMC PARENT HEBER SPRINGS'
		WHEN BH_STAT_COSTCENTERS.COST_CENTER_DESC LIKE '800%' THEN 'BHMC PARENT EXTENDED CARE'
		ELSE 'NOT 100-800'
		END AS LOCATION_DESCRIPTION
	,'HK' AS ACCT_BASE_CLASS
	,'HAND KEYED' AS SERVICE_LINE_BASIC
	,'HAND KEYED' AS SERVICE_LINE_DETAIL
	,BH_STAT_HK_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,SUM(TOTAL_QUANTITY) AS TOTAL_QUANTITY
	,SUM(TOTAL_QUANTITY) AS TOTAL_WEIGHT
	,BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE AS WEIGHT_TYPE_COSTCENTER
FROM BH_STAT_HK_CHARGES
	LEFT OUTER JOIN BH_STAT_COSTCENTERS ON
		BH_STAT_HK_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTERS.COST_CENTER_CODE
	INNER JOIN BH_STAT_COSTCENTER_TYPES ON
		BH_STAT_HK_CHARGES.COST_CENTER_CODE = BH_STAT_COSTCENTER_TYPES.COST_CENTER_CODE
WHERE
	SERVICE_DATE BETWEEN @begin AND @end
	--SERVICE_DATE BETWEEN '2014-01-01' AND '2014-06-30'
GROUP BY
	BH_STAT_HK_CHARGES.COST_CENTER_CODE
	,BH_STAT_COSTCENTERS.COST_CENTER_DESC
	,BH_STAT_COSTCENTER_TYPES.WEIGHT_TYPE

SELECT
	*
FROM #STATS
ORDER BY
	COST_CENTER_CODE

DROP TABLE #STATS
DROP TABLE #PISDISCHARGES
DROP TABLE #PATIENTS
DROP TABLE #CASES
DROP TABLE #VISITS
