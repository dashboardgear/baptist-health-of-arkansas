SELECT DISTINCT
	CHARGE_TYPE,
	CASE
		WHEN CHARINDEX('RVU',CHARGE_TYPE)>0
			THEN 'RVU'
		WHEN CHARINDEX('APC',CHARGE_TYPE)>0
			THEN 'APC'
		WHEN CHARINDEX('BTU',CHARGE_TYPE)>0
			THEN 'BTU'
		WHEN CHARINDEX('Day',CHARGE_TYPE)>0
			THEN 'PT_DAY'
		WHEN CHARINDEX('MINUTE',CHARGE_TYPE)>0
			THEN 'MINUTE'
		WHEN CHARINDEX('N/A',CHARGE_TYPE)>0
			THEN 'N/A'
		ELSE 'COUNT'
	END AS WEIGHT_TYPE
FROM BH_STAT_CHG_WEIGHT