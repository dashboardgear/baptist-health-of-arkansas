/*	FinStat_EXPENSES.sql
*/

SELECT
	CASE RTRIM(ACCT_UNIT_LEVEL1_NAME)
		WHEN 'BHMC-Arkadelphia' THEN 'BHMC PARENT ARKADELPHIA'
		WHEN 'BHMC Extended Care - LR' THEN 'BHMC PARENT EXTENDED CARE'
		WHEN 'BHMC-Heber Springs' THEN 'BHMC PARENT HEBER SPRINGS'
		WHEN 'BHMC-Little Rock' THEN 'BHMC PARENT LITTLE ROCK'
		WHEN 'BHMC-North Little Rock' THEN 'BHMC PARENT NORTH LITTLE ROCK'
		WHEN 'BH Rehabilitation Institute' THEN 'BHMC PARENT REHABILITATION INSTITUTE'
		WHEN 'BHMC-Stuttgart' THEN 'BHMC PARENT STUTTGART'
		ELSE ACCT_UNIT_LEVEL1_NAME
	END AS LOCATION_DESCRIPTION,
	SUM(AMOUNT) AS EXPENSES
	
  FROM GLREPORTINGBYPERIOD
  WHERE CHART_LEVEL1_NAME = 'EXPENSES'
	AND ACCT_UNIT_LEVEL1 IN ('100','200','300','400','500','600','800')
	AND FISCAL_YEAR=2014
	AND PERIOD=6
  GROUP BY ACCT_UNIT_LEVEL1_NAME

SELECT
	FISCAL_YEAR,
	PERIOD,
	PERIOD_END_DATE
	
  FROM GLREPORTINGBYPERIOD
  WHERE CHART_LEVEL1_NAME = 'EXPENSES'
	AND ACCT_UNIT_LEVEL1 IN ('100','200','300','400','500','600','800')
	AND FISCAL_YEAR=2014
	AND PERIOD=6
  GROUP BY ACCT_UNIT_LEVEL1_NAME


--SELECT DISTINCT
--	ACCT_UNIT_LEVEL1,
--	ACCT_UNIT_LEVEL1_NAME
--FROM GLREPORTINGBYPERIOD
--WHERE 
--	ACCT_UNIT_LEVEL1_NAME LIKE 'BHMC%'
--	OR ACCT_UNIT_LEVEL1_NAME LIKE '%REHAB%'
--ORDER BY ACCT_UNIT_LEVEL1