/*	Find Detail Service Lines.sql
*/

CREATE TABLE #LIST (
	DET_SL varchar(40) null)

INSERT INTO #LIST (DET_SL)
VALUES  ('Obstetrics'), ('Gynecology'), ('NICU'), ('Cardiology Medical'), ('Cardiac Cath'), ('Cardiac EP'), ('Cardiac Surgery'), ('Vascular Services'), ('Thoracic Surgery'), ('Orthopedics Surgery'), ('Orthopedics Medical'), ('Neurosurgery'), ('Spine'), ('Neurology'), ('General Surgery'), ('Gastroenterology'), ('Bariatrics'), ('ENT'), ('Oncology'), ('General Medicine'), ('Stroke and Transient Ischemic Attack'), ('Trauma'), ('Pulmonology'), ('Urology'), ('Nephrology'), ('Ophthalmology'), ('Transplant Heart / VAD'), ('Transplant Kidney'), ('Psychiatry'), ('Substance Abuse'), ('Geri-Psych'), ('Unassigned'), ('Rehab-Stroke'), ('Rehab-Fractures'), ('Rehab-Total Joint Replacement'), ('Rehab-Debility'), ('Rehab-Neurological Disorders'), ('Rehab-Spine / Brain Injury'), ('Unassigned'), ('SNF / Swingbed'), ('LTACH - Vent 96 Hours+'), ('LTACH - Other'), ('Unassigned'), ('Inpatient Newborn'), ('Cardiac EP'), ('Cardiac Cath'), ('Neurosurgery'), ('Orthopedics'), ('Ophthalmology'), ('Urology'), ('Plastic Surgery'), ('OB and GYN'), ('Ear, Nose, and Throat'), ('Interventional Radiology'), ('General Surgery'), ('Vascular'), ('Gastroenterology'), ('Cardiac Surgery'), ('Wound Care'), ('Sleep Center'), ('Therapy - PT/OT/ST'), ('Observation'), ('Radiology - PET'), ('Radiology - MRI'), ('Radiology - Nuclear Medicine'), ('Radiology - CT'), ('Radiology - Mammography'), ('Radiology - Radiation Oncology'), ('Chemotherapy'), ('Cardiology Medical'), ('Radiology - Ultrasound'), ('Radiology - Bone Density'), ('Neurology'), ('Radiology - Other Diagnostic'), ('Respiratory Therapy'), ('Blood Bank'), ('Laboratory'), ('Other'), ('Unassigned'), ('Emergency Dept'),
	 ('Ambulance'),  ('Dental'),  ('Devices'),  ('Diabetes Management'),  ('Dialysis'),  ('DME and Supplies'),  ('Drugs/Biologicals'),  ('Emergency Department Visit'),  ('Emergency Department Visit - Type B'),  ('Immunizations'),  ('Injectables'),  ('Radiopharmaceuticals'),  ('Sleep Study'),  ('Vision'),  ('Wound Care - Hyperbaric');


SELECT DISTINCT OP_DETAIL_SERVICE_LINE
FROM BH_SL_CPT_REFERENCE
WHERE
	OP_DETAIL_SERVICE_LINE NOT IN (SELECT DET_SL FROM #LIST)
	
DROP TABLE #LIST