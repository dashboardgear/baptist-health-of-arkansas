/*	BuildPatientTable_rev21.sql
	5/6/2014 pb
	Generate BH_RPT_PATIENTS
	-5/28/14 Use LTACH Reference table for DRG data if Location is 10205 or 10102
	-5/30/14 Set Volume Flags from charges data
	-6/2/14  Create temp table with total charges for each EPC, DEFAULT_REV_CODE; lookup from temp table
		rather than BH_EPIC_CHARGES
	-6/4/14 Use DEFAULT_REV_CODE from BH_EPIC_CHARGES instead of REV_CODE
		- Remove records where DISCHARGE_DEPT_CODE in list of clinics, other non-hospital locations
	-6/5/14 Revise exclusion of non-hospital locations to retain NULL values
	-6/6/14 Add calculation of Basic Service Line
	-6/7/14 Add calculation of Detail Service Line
	-6/9/14 Added Vein Center to block of clinic charges considered first for assignment of service line
	-6/10/14 Enhanced Basic Service Line calcs with separate temp file, which is then referenced by
		Detail Service Line calcs for four OP clinic service lines + Observation
	-6/18/14 Added ADJ_PAYMENTS_NO_CR field; Removed ADJ_ADJUSTMENTS_NO_CR (per Andrew)
	-6/20/14 Modified ADJ_PAYMENTS_NO_CR to use TOT_PYMTS instead of TOTAL_PATIENT_PAYMENTS
	-6/24/14 Modified calculation of SERVICE_LINE_BASIC
		Created temp table for Charges that excludes specific DEFAULT_REV_CODEs
	-6/25/14 Do NOT eliminate supply-type Default Rev Codes for OP Basic Service Line calculations
		Modifications to Basic Service Line calcs for NICU, added Newborns
	-6/26/14 Add lookup to BH_INSURANCE_GROUPS_REFRENCE to populate fields DS_FINANCIAL_CLASS, DS_PAYER_CATEGORY
		and DS_INSURANCE_GROUP (first two are new). Drop field INSURANCE_GROUP_NBR
		Added "Breast Center" to hierarchy for Basic Service Line
	-6/27/14 Changed charges selection logic. Excluded service_type charges from #CHARGES_TEMP_ALL (from which all _OP temp tables are built), 
		and included ONLY Room-type charges in #CHARGES_TEMP_IP (from which all _IP temp tables are built)
	-7/1/14 Removed RevCode 260 (IV Infusion) from excluded charges when building #CHARGES_TEMP_ALL (per Andrew)
	-7/19/14 Change basic service line hierarchy to put Observation before PT,ST,OT (per Brock)
		For Detail Service Line, do not re-use Basic hierarchy, only check for Basic SL in selected group
	-7/22/14 Added new field INSTANT_OF_UPDATE
	-8/1/14 Changed lookup to DRG, CMG and LTACH Reference tables to use CONVERT(DATE,PAT.DISCHARGE_DATETIME) 
			rather than ADMIT_DATE
	-8/5/14 Removed conversion of LTACH_DRG_WEIGHT to 2 decimals leaving it as decimal(8,4)
*/

/*******************************
Build temp tables with totals by ACCOUNT_ID, EPC; ACCOUNT_ID, DEFAULT_REV_CODE;
ACCOUNT_ID, CPT; Create #BASIC_SERVICE_LINES table for lookup during DETAIL_SERVICE_LINE section
Use query SelectCharges.sql
*******************************/
----- Build temp CHARGES table with certain default Rev Codes excluded (6/24/14) -----
SELECT
	ACCOUNT_ID,
	EXTERNAL_PROCEDURE_CODE,
	COST_CENTER_CODE,
	DEFAULT_REV_CODE,
	AMOUNT
INTO #CHARGES_TEMP_ALL
FROM BH_EPIC_CHARGES
WHERE
	(DEFAULT_REV_CODE NOT BETWEEN 250 AND 259   --260 (260 removed per Andrew 7/1/14)
		AND DEFAULT_REV_CODE NOT BETWEEN 270 AND 279
		AND DEFAULT_REV_CODE NOT BETWEEN 621 AND 637)  --Exclude supply-type charges

SELECT
	ACCOUNT_ID,
	EXTERNAL_PROCEDURE_CODE,
	COST_CENTER_CODE,
	DEFAULT_REV_CODE,
	AMOUNT
INTO #CHARGES_TEMP_IP
FROM #CHARGES_TEMP_ALL
WHERE
	DEFAULT_REV_CODE BETWEEN 100 AND 219  --Room charges only

-----------------------------	

SELECT --TOP 100
	ACCOUNT_ID,
	EXTERNAL_PROCEDURE_CODE,
	COST_CENTER_CODE,
	SUM(AMOUNT) AS TOT_AMT
INTO #EPC_TEMP
FROM #CHARGES_TEMP_ALL  --BH_EPIC_CHARGES
WHERE
	EXTERNAL_PROCEDURE_CODE IN ('45000001', '45000002', '45000003', '45000004', '45000005', '45000006', '45000015','54000002')
GROUP BY ACCOUNT_ID, EXTERNAL_PROCEDURE_CODE, COST_CENTER_CODE
HAVING SUM(AMOUNT)>0
----------------------------------

SELECT --TOP 100
	ACCOUNT_ID,
	DEFAULT_REV_CODE,
	SUM(AMOUNT) AS TOT_AMT
INTO #REV_TEMP_IP
FROM #CHARGES_TEMP_IP  --BH_EPIC_CHARGES
GROUP BY ACCOUNT_ID, DEFAULT_REV_CODE
HAVING SUM(AMOUNT)>0

SELECT --TOP 100
	ACCOUNT_ID,
	DEFAULT_REV_CODE,
	SUM(AMOUNT) AS TOT_AMT
INTO #REV_TEMP_OP
FROM #CHARGES_TEMP_ALL  --BH_EPIC_CHARGES
WHERE
	DEFAULT_REV_CODE IN ('0681', '0682', '0683', '0684', '0689','762')
GROUP BY ACCOUNT_ID, DEFAULT_REV_CODE
HAVING SUM(AMOUNT)>0

----------------------------------
/*	Build temp tables from BH_EPIC_CHARGES
*/

SELECT
	ACCOUNT_ID,
	COST_CENTER_CODE
INTO #CC_TEMP_IP
FROM #CHARGES_TEMP_IP  --BH_EPIC_CHARGES
GROUP BY ACCOUNT_ID, COST_CENTER_CODE
HAVING SUM(AMOUNT)>0;

SELECT
	ACCOUNT_ID,
	COST_CENTER_CODE
INTO #CC_TEMP_OP
FROM #CHARGES_TEMP_ALL  --BH_EPIC_CHARGES
GROUP BY ACCOUNT_ID, COST_CENTER_CODE
HAVING SUM(AMOUNT)>0;

--- Below index reduces execution time 
--- because of multiple table scans in CASE statements below
CREATE NONCLUSTERED INDEX IDX_CC_TEMP
ON #CC_TEMP_OP (ACCOUNT_ID, COST_CENTER_CODE)

----------------------------------------
/*	Set up data for Basic Service Lines */
----------------------------------------
----------- SERVICE_LINE_BASIC ---------
SELECT
ACCOUNT_ID,
ACCT_BASE_CLASS,
CASE ACCT_BASE_CLASS
WHEN 'IP' THEN 
	CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN 'LTACH'
		WHEN PAT.LOCATION = '10201' OR PAT.DISCH_DEPT_CODE = '10002128' THEN 'Rehab'
		WHEN PAT.DISCH_DEPT_CODE IN ('10003110', '10004145','10101253','10005147') THEN 'SNF/Swingbed'
		WHEN PAT.DISCH_DEPT_CODE = '10101241' THEN 'Geri-Psych'
		WHEN ((EXISTS(SELECT COST_CENTER_CODE FROM #CC_TEMP_IP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE='3006123')))
			THEN 'NICU'
		WHEN (EXISTS(SELECT DEFAULT_REV_CODE FROM #REV_TEMP_IP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND DEFAULT_REV_CODE IN 
				('170','171','172','173','174','179'))) THEN 'Newborn'  --Not in NICU
		ELSE 
			ISNULL(DRG.BASIC_SERVICE_LINE,'Unassigned')   --DRG.BASIC_SERVICE_LINE
	END 
WHEN 'OP' THEN
	CASE --First match based on hierarchy
		WHEN (NOT EXISTS (SELECT ACCOUNT_ID FROM #CC_TEMP_OP)) AND (CHG_O.DEFAULT_REV_CODE!='762') THEN NULL
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Cardiac Cath/EP')))
			THEN 'Cardiac Cath/EP'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Operating Room')))
			THEN 'Operating Dept'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='GI Lab')))
			THEN 'GI Lab'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Interventional Radiology')))
			THEN 'Interventional Radiology'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Vein Center')))
			THEN 'Vein Center'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Sleep Lab')))
			THEN 'Sleep Lab'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Wound Center')))
			THEN 'Wound Center'
		WHEN CHG_O.DEFAULT_REV_CODE = '762' THEN 'Observation' --Moved up before PT,OT,ST 7/19/14 (per Brock)
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='PT,OT,ST')))
			THEN 'PT,OT,ST'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Breast Center')))
			THEN 'Breast Center'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Radiology')))
			THEN 'Radiology'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Other')))
			THEN 'Other'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='Laboratory')))
			THEN 'Laboratory'
		WHEN (EXISTS (SELECT COST_CENTER_CODE FROM #CC_TEMP_OP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE IN (SELECT COST_CENTER FROM BH_SL_COSTCENTER_REFERENCE WHERE OP_BASIC_SERVICE_LINE='N/A')))
			THEN 'N/A'		
		ELSE 'Error'
		END
WHEN 'ED' THEN 'ED'
END AS SERVICE_LINE_BASIC

INTO #BASIC_SERVICE_LINES
FROM BH_EPIC_PATIENTS  PAT
	OUTER APPLY (SELECT TOP 1 DEFAULT_REV_CODE FROM #REV_TEMP_OP  --BH_EPIC_CHARGES 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
			DEFAULT_REV_CODE = '762' -- Observation
		) AS CHG_O
	OUTER APPLY (SELECT DRG, DRG_DESCRIPTION, DRG_WEIGHT, DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_DRG_REFERENCE WHERE 
		DRG=PAT.DRG AND PAT.LOCATION NOT IN ('10205','20102')
		AND ISNULL(PAT.ADMIT_DATE,PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) DRG
	OUTER APPLY (SELECT LTC_DRG, LTC_DRG_DESCRIPTION, LTC_DRG_WEIGHT, LTC_DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_LTACH_REFERENCE WHERE 
		LTC_DRG=PAT.DRG AND PAT.LOCATION IN ('10205','20102')
		AND ISNULL(PAT.ADMIT_DATE,PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) LTC
WHERE 
	PAT.INCLUDE_FLAG='Y'
	AND (DISCH_DEPT_CODE IS NULL OR 
		DISCH_DEPT_CODE NOT IN ('10101201', '10004102', '10003163', '10005160', '10005161', '10005162', '10005163', '10005157', '10005129', '10003138', '10101466', '10002212','10005165'))

----------------------------------------
/*	Set up data for Detail Service Lines */
----------------------------------------
/*	BuildRankedCPTListByAcct.sql
*/
SELECT
	ACCOUNT_ID,
	CPT_HCPCS_CODE
INTO #CPT_T1
FROM BH_EPIC_CPT  --Was _CHARGES Table
GROUP BY ACCOUNT_ID, CPT_HCPCS_CODE;

WITH cte AS (
SELECT 
	t1.ACCOUNT_ID,
	t1.CPT_HCPCS_CODE,
	t3.DETAIL_SERVICE_LINE,
	t3.RANKING,
	ROW_NUMBER() OVER (PARTITION BY t1.ACCOUNT_ID ORDER BY t1.ACCOUNT_ID, t3.RANKING) AS RowNum
FROM #CPT_T1 t1
	LEFT OUTER JOIN BH_SL_CPT_REFERENCE t2 ON
		t2.HCPCS_CODE=t1.CPT_HCPCS_CODE
	LEFT OUTER JOIN BH_SL_DETAIL_RANK t3 ON
		t3.DETAIL_SERVICE_LINE=t2.OP_DETAIL_SERVICE_LINE
WHERE
	t1.CPT_HCPCS_CODE IS NOT NULL
	AND t3.RANKING IS NOT NULL
)
SELECT 
	ACCOUNT_ID,
	CPT_HCPCS_CODE AS CPT_CODE,
	DETAIL_SERVICE_LINE,
	RANKING
INTO #CPT_TEMP
FROM cte
WHERE RowNum = 1

DROP TABLE #CPT_T1

/*******************************
Begin main query
*******************************/

SELECT --TOP 200 
      PAT.ACCOUNT_ID
	  ,PAT.LOCATION
	  ,LOC.LOCATION_DESCRIPTION
	  ,CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN LTC.LTC_DRG
		ELSE PAT.DRG
		END AS DRG
	  ,CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN LTC.LTC_DRG_DESCRIPTION
		ELSE DRG.DRG_DESCRIPTION
		END AS DRG_DESCRIPTION
	  ,CMG_CODE
	  ,CMG_DESCRIPTION
      ,PAT.MRN
      ,PAT.AGE
      ,PAT.ZIP
      ,PAT.LOS_IP_ADMIT
      ,PAT.LOS_ADMIT
      ,CASE
		WHEN PAT.ACCT_BASE_CLASS = 'IP' THEN PAT.IP_ADMIT_DATETIME
		ELSE PAT.ADMIT_DATETIME
		END AS BILLING_ADMIT_DATE
      ,PAT.ADMIT_DATETIME
      ,PAT.IP_ADMIT_DATETIME
      ,PAT.DISCHARGE_DATETIME
      ,PAT.ACCT_BASE_CLASS
      ,PAT.DISCH_DEPT_CODE
      ,PAT.DISCH_DEPT_DESC
      ,PAT.DISCHARGE_CODE
      ,PAT.DISCH_CODE_DESC
      ,PAT.TOTAL_CHARGES
      ,PAT.TOTAL_PATIENT_PAYMENTS
      ,(PAT.TOT_PMTS + PAT.TOTAL_REFUNDS) AS ADJ_PAYMENTS_NO_CR  --Modified 6/24/14
      ,(PAT.TOT_ADJUSTMENTS - PAT.TOTAL_REFUNDS) AS ADJ_ADJUSTMENTS  --ADJ are negative, Refunds positive
  --    ,(PAT.TOT_ADJUSTMENTS + PAT.TOTAL_REFUNDS)+ 
		--(SELECT SUM(AMOUNT) FROM BH_EPIC_TRANSACTIONS
		--	WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND TRANSACTION_TYPE=4) AS ADJ_ADJUSTMENTS_NO_CR
      ,PAT.TOTAL_REFUNDS
      ,PAT.ACCOUNT_BALANCE
      ,PAT.PRIMARY_FINANCIAL_CLASS
      ,PAT.PRIMARY_FINANCIAL_CLASS_DESC
      ,PAT.PRIMARY_INSURANCE_PLAN
      ,PAT.PRIMARY_INSURANCE_PLAN_DESC
      ,PTX_A.PHYSICIAN_ID AS ADMITTING_PHYSICIAN_CODE
      ,CASE
		WHEN PM_A.FIRST_NAME IS NULL THEN PM_A.LAST_NAME
		WHEN PM_A.MIDDLE_NAME IS NULL THEN PM_A.FIRST_NAME+' '+PM_A.LAST_NAME
		ELSE PM_A.FIRST_NAME+' '+PM_A.MIDDLE_NAME+' '+PM_A.LAST_NAME
		END AS ADMITTING_PHYSICIAN_NAME
       ,PTX_P.PHYSICIAN_ID AS PCP_PHYSICIAN_CODE
      ,CASE
		WHEN PM_P.FIRST_NAME IS NULL THEN PM_P.LAST_NAME
		WHEN PM_P.MIDDLE_NAME IS NULL THEN PM_P.FIRST_NAME+' '+PM_P.LAST_NAME
		ELSE PM_P.FIRST_NAME+' '+PM_P.MIDDLE_NAME+' '+PM_P.LAST_NAME
		END AS PCP_PHYSICIAN_NAME
       ,PTX_R.PHYSICIAN_ID AS REFERRING_PHYSICIAN_CODE
      ,CASE
		WHEN PM_R.FIRST_NAME IS NULL THEN PM_R.LAST_NAME
		WHEN PM_R.MIDDLE_NAME IS NULL THEN PM_R.FIRST_NAME+' '+PM_R.LAST_NAME
		ELSE PM_R.FIRST_NAME+' '+PM_R.MIDDLE_NAME+' '+PM_R.LAST_NAME
		END AS REFERRING_PHYSICIAN_NAME
	  ,CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN LTC.LTC_DRG_WEIGHT  --CONVERT(decimal(8,2),LTC.LTC_DRG_WEIGHT)
		ELSE DRG.DRG_WEIGHT
		END AS DRG_WEIGHT
	  ,CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN LTC.LTC_DRG_GLOS
		ELSE DRG.DRG_GLOS
		END AS DRG_GLOS
	  ,CASE
		WHEN PAT.LOCATION IN ('10205','20102') THEN CONVERT(DECIMAL(8,4),(PAT.LOS_IP_ADMIT - LTC.LTC_DRG_GLOS))
		ELSE CONVERT(DECIMAL(8,4),(PAT.LOS_IP_ADMIT - DRG.DRG_GLOS))
		END AS GLOS_VARIANCE
	  ,CMG.WEIGHT AS CMG_WEIGHT
	  ,CMG.ALOS AS CMG_ALOS
      ,PAT.PATIENT_CLASS_CODE
      ,PAT.PATIENT_CLASS_DESC
      ,PAT.PATIENT_STATUS  --
      ,0 AS TOTAL_COST
      ,0 AS VARIABLE_COST
      ,0 AS INDIRECT_COST
      ,0 AS FIXED_COST
      --,0 AS INSURANCE_GROUP_NBR
      --,'' AS INSURANCE_GROUP_NAME
      ,BHI.DS_FINANCIAL_CLASS
      ,BHI.DS_PAYER_CATEGORY
      ,BHI.INSURANCE_GROUP

      ----------------- SERVICE_LINE_BASIC ----------------
      ,BSL.SERVICE_LINE_BASIC

      ----------------- SERVICE_LINE_DETAIL ----------------
      ,CASE PAT.ACCT_BASE_CLASS
		WHEN 'IP' THEN 
			CASE
				WHEN PAT.DISCH_DEPT_CODE IN ('10003110', '10004145','10101253','10005147') THEN 'SNF/Swingbed'
				WHEN PAT.DISCH_DEPT_CODE = '10101241' THEN 'Geri-Psych'
				WHEN ((EXISTS(SELECT COST_CENTER_CODE FROM #CC_TEMP_IP WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND COST_CENTER_CODE='3006123')))
					THEN 'NICU'
				WHEN BSL.SERVICE_LINE_BASIC='Observation' THEN 'Observation'
			ELSE
				CASE
					WHEN PAT.LOCATION = '10201' OR PAT.DISCH_DEPT_CODE = '10002128' --Rehab
						THEN CMG.DETAIL_SERVICE_LINE
					WHEN PAT.LOCATION IN ('10205','20102') THEN LTC.DETAIL_SERVICE_LINE --LTACH
					ELSE DRG.DETAIL_SERVICE_LINE
				END
			END 
		WHEN 'OP' THEN
			CASE --First match based on SERVICE_LINE_BASIC hierarchy
				--WHEN BSL.SERVICE_LINE_BASIC='Vein Center' THEN 'Vein Center'
				--WHEN BSL.SERVICE_LINE_BASIC='Wound Center' THEN 'Wound Center'
				--WHEN BSL.SERVICE_LINE_BASIC='Sleep Lab' THEN 'Sleep Lab'
				--WHEN BSL.SERVICE_LINE_BASIC='Observation' THEN 'Observation'
				--WHEN BSL.SERVICE_LINE_BASIC='PT,OT,ST' THEN 'PT,OT,ST'
				WHEN BSL.SERVICE_LINE_BASIC IN ('Vein Center','Wound Center','Sleep Lab','Observation','PT,OT,ST')
					THEN BSL.SERVICE_LINE_BASIC
			ELSE
				CPT.DETAIL_SERVICE_LINE
			END
		WHEN 'ED' THEN 'ED'
	  END AS SERVICE_LINE_DETAIL
	      
      ,CASE
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE IN ('45000001', '45000002', '45000003', '45000004', '45000005', '45000006', '45000015') --ED
			THEN 1
		ELSE 0
		END AS VOLUME_FLAG_ED
	  ,CASE
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000001' THEN 'Level 1'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000002' THEN 'Level 2'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000003' THEN 'Level 3'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000004' THEN 'Level 4'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000005' THEN 'Level 5'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000006' THEN 'Critical Care'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE = '45000015' THEN 'Medicaid Screen'
		ELSE NULL
	   END AS VOLUME_ED_LEVEL
	  ,CASE
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE IN ('45000001', '45000002', '45000003', '45000004', '45000005', '45000006', '45000015') --ED
			AND CHG_ED.COST_CENTER_CODE='3007030' THEN 'After Hours Peds'
		WHEN CHG_ED.EXTERNAL_PROCEDURE_CODE IN ('45000001', '45000002', '45000003', '45000004', '45000005', '45000006', '45000015') --ED
			AND CHG_ED.COST_CENTER_CODE!='3007030' THEN 'Emergency Dept'
		ELSE NULL
	   END AS VOLUME_ED_SETTING
	  ,CASE
		WHEN CHG_T.DEFAULT_REV_CODE IN ('0681', '0682', '0683', '0684', '0689') --Trauma
			THEN 1
		ELSE 0
	   END AS VOLUME_FLAG_TRAUMA
	  ,CASE
		WHEN CHG_MF.EXTERNAL_PROCEDURE_CODE='54000002'  --MedFlight
			THEN 1
		ELSE 0
	   END AS VOLUME_FLAG_MEDFLIGHT
	  ,CASE
		WHEN CHG_O.DEFAULT_REV_CODE = '762' -- Observation
			THEN 1
		ELSE 0
	   END AS VOLUME_FLAG_OBSERVATION
      ,1 AS CASE_COUNT
	  ,PAT.INSTANT_OF_UPDATE
      
INTO #t1
FROM BH_EPIC_PATIENTS PAT
	LEFT OUTER JOIN BH_EPIC_LOCATIONS LOC ON
		LOC.LOCATION=PAT.LOCATION
	LEFT OUTER JOIN BH_EPIC_PHYSICIANS_TX PTX_A ON
		PTX_A.ACCOUNT_ID=PAT.ACCOUNT_ID
		AND PTX_A.ROLE='Admit'
	LEFT OUTER JOIN BH_EPIC_PHYSICIANS_TX PTX_P ON
		PTX_P.ACCOUNT_ID=PAT.ACCOUNT_ID
		AND PTX_P.ROLE='PCP'
	LEFT OUTER JOIN BH_EPIC_PHYSICIANS_TX PTX_R ON
		PTX_R.ACCOUNT_ID=PAT.ACCOUNT_ID
		AND PTX_R.ROLE='Refer'
	LEFT OUTER JOIN BH_EPIC_PHYSICIAN_MASTER PM_A ON
		PM_A.PID=PTX_A.PHYSICIAN_ID
	LEFT OUTER JOIN BH_EPIC_PHYSICIAN_MASTER PM_P ON
		PM_P.PID=PTX_P.PHYSICIAN_ID
	LEFT OUTER JOIN BH_EPIC_PHYSICIAN_MASTER PM_R ON
		PM_R.PID=PTX_R.PHYSICIAN_ID
	
	OUTER APPLY (SELECT DRG, DRG_DESCRIPTION, DRG_WEIGHT, DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_DRG_REFERENCE WHERE 
		DRG=PAT.DRG AND PAT.LOCATION NOT IN ('10205','20102')
		--AND ISNULL(PAT.ADMIT_DATE,PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) DRG
		AND ISNULL(CONVERT(DATE,PAT.DISCHARGE_DATETIME),PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) DRG
	OUTER APPLY (SELECT LTC_DRG, LTC_DRG_DESCRIPTION, LTC_DRG_WEIGHT, LTC_DRG_GLOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_LTACH_REFERENCE WHERE 
		LTC_DRG=PAT.DRG AND PAT.LOCATION IN ('10205','20102')
		AND ISNULL(CONVERT(DATE,PAT.DISCHARGE_DATETIME),PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) LTC

	OUTER APPLY (SELECT CMG, CMG_DESCRIPTION, WEIGHT, ALOS, BASIC_SERVICE_LINE, DETAIL_SERVICE_LINE FROM BH_CMG_REFERENCE WHERE 
		CMG=PAT.CMG_CODE
		AND ISNULL(CONVERT(DATE,PAT.DISCHARGE_DATETIME),PAT.SOURCE_DATE) BETWEEN EFFECTIVE_BEGIN AND EFFECTIVE_END) CMG

	OUTER APPLY (SELECT TOP 1 EXTERNAL_PROCEDURE_CODE, COST_CENTER_CODE FROM #EPC_TEMP 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
		EXTERNAL_PROCEDURE_CODE IN ('45000001', '45000002', '45000003', '45000004', '45000005', '45000006', '45000015') --ED
		ORDER BY
			CASE
				WHEN EXTERNAL_PROCEDURE_CODE='45000006' THEN 1
				WHEN EXTERNAL_PROCEDURE_CODE='45000005' THEN 2
				WHEN EXTERNAL_PROCEDURE_CODE='45000004' THEN 3
				WHEN EXTERNAL_PROCEDURE_CODE='45000003' THEN 4
				WHEN EXTERNAL_PROCEDURE_CODE='45000002' THEN 5
				WHEN EXTERNAL_PROCEDURE_CODE='45000001' THEN 6
				WHEN EXTERNAL_PROCEDURE_CODE='45000015' THEN 7
			END
		) AS CHG_ED
	OUTER APPLY (SELECT TOP 1 EXTERNAL_PROCEDURE_CODE FROM #EPC_TEMP --BH_EPIC_CHARGES 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
			EXTERNAL_PROCEDURE_CODE='54000002'  --MedFlight
		) AS CHG_MF
	OUTER APPLY (SELECT TOP 1 DEFAULT_REV_CODE FROM #REV_TEMP_OP  --BH_EPIC_CHARGES 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
			DEFAULT_REV_CODE IN ('0681', '0682', '0683', '0684', '0689') --Trauma
		) AS CHG_T
	OUTER APPLY (SELECT TOP 1 DEFAULT_REV_CODE FROM #REV_TEMP_OP   --BH_EPIC_CHARGES 
		WHERE ACCOUNT_ID=PAT.ACCOUNT_ID AND
			DEFAULT_REV_CODE = '762' -- Observation
		) AS CHG_O
	LEFT OUTER JOIN #BASIC_SERVICE_LINES BSL ON
		BSL.ACCOUNT_ID=PAT.ACCOUNT_ID
	LEFT OUTER JOIN #CPT_TEMP CPT ON
		CPT.ACCOUNT_ID=PAT.ACCOUNT_ID
	LEFT OUTER JOIN BH_INSURANCE_GROUP_REFERENCE BHI ON
		BHI.BENEFIT_PLAN_ID=PAT.PRIMARY_INSURANCE_PLAN

WHERE 
	PAT.INCLUDE_FLAG='Y'
	AND (DISCH_DEPT_CODE IS NULL OR 
		DISCH_DEPT_CODE NOT IN ('10101201', '10004102', '10003163', '10005160', '10005161', '10005162', '10005163', '10005157', '10005129', '10003138', '10101466', '10002212','10005165'))
	
ORDER BY PAT.ACCOUNT_ID

TRUNCATE TABLE BH_RPT_PATIENTS
INSERT INTO BH_RPT_PATIENTS
SELECT *
FROM #t1

DROP TABLE #t1
DROP TABLE #CHARGES_TEMP_ALL
DROP TABLE #CHARGES_TEMP_IP
DROP TABLE #EPC_TEMP
DROP TABLE #REV_TEMP_OP
DROP TABLE #REV_TEMP_IP
DROP TABLE #CC_TEMP_IP
DROP TABLE #CC_TEMP_OP
DROP TABLE #CPT_TEMP
DROP TABLE #BASIC_SERVICE_LINES
